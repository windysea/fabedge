# FabEdge介绍
![build](https://github.com/FabEdge/fabedge/actions/workflows/main.yml/badge.svg)
![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)

FabEdge是一款基于kubernetes和kubeedge构建的开源网络方案，解决边缘计算场景下，容器网络配置管理复杂、网络割裂互不通信、缺少服务发现、缺少拓扑感知能力、无法提供就近访问等问题。Fabedge支持弱网环境，如4/5G，WiFi，LoRa等；支持边缘节点动态IP地址，适用于物联网，车联网等场景。

# 特性
* Kubernetes 原生支持：完全兼容的 Kubernetes API ，无需额外开发，依赖较少的通用开源组件，即插即用。
* 边缘容器网络管理：为边缘节点实现网络地址段管理，以及边缘容器网络地址分配。
* 边云协同/边边协同：通过隧道技术打通边缘容器与云端容器，以及边缘节点间容器的相互安全通信，实现边云协同和边边协同。
* 边缘“社区”：使用“社区”CRD自定义资源控制哪些边缘节点可以互相通讯。
* 就近访问：优先使用本地服务，其次使用云端服务。

# 优势
标准 -- 完全兼容k8s api, 即插即用支持任何标准k8s集群。

安全  -- 所有通讯使用基于证书的IPSEC隧道。

易用  -- 使用Operator机制，最少化的人工运维代价。

# FabEdge架构实现
云端是标准Kubernete集群，可以使用任何CNI网络插件，比如Calico。在集群里运行Kubeedge 云端组件cloudcore，在边缘节点运行Kubeedge边缘组件edgecore，边缘节点注册到云端集群。
 
Fabedge有三个组件组成，分别是：Operator， Connector和Agent。 

· Operator运行在云端集群，监控节点，服务等资源变化，动态为边缘节点维护configmap，同时为每个边缘节点生成Agent。
 
· Agent消费configmap信息，动态维护本节点隧道，路由，iptables等网络配置。
 
· Connector运行在云端，负责云端网络配置管理，并在云端和边缘节点转发流量。 

![image](https://user-images.githubusercontent.com/88021699/127309439-277bb003-5d9c-4eaf-af4f-0cd1f28158e5.png)

· FabEdge利用两个通道实现云边数据交换，一个是kubeedge管理的websock/quic通道，用于控制信令；另一个是FabEdge自身管理的加密隧道，用于应用之间的数据传输。

· Operator在云端监听node、service、endpoint等k8s资源，为每个边缘节点生成一个configmap，包含本节点的子网、隧道、负载均衡等相关配置信息。同时operator负责为每个边缘节点生成相应的pod，用于启动本节点上的agent。

· Connector负责终结到边缘节点加密隧道，在云和边缘节点进行流量转发。它依赖云端CNI插件将流量转发到connector以外的节点，目前支持的云端CNI插件是callico。

· 边缘节点使用社区CNI插件bridge和host-local。

· 边缘节点是使用社区node-local-dns地址解析功能，负责本节点的域名解析和缓存。 
  每个边缘节点运行一个agent，消费本节点对应configmap，包括以下功能：

  1)管理本节点CNI插件的配置文件

  2)管理本节点安全隧道

  3)管理本节点的负载均衡信息，会优先使用本地服务后端，其次是用云端后端

# FabEdge vs Calico、Flannel 
Fabedge不同与Calico，Flannel等标准Kubernetes网络插件。这些插件主要应用在数据中心，解决kubernetes集群内部网络问题，而Fabedge解决的是边缘计算场景下，使用Kubeedge将边缘节点接入云端Kubernetes集群后，不同边缘节点上POD之间， 边缘节点POD和云端POD之间如何通讯的问题。 目前Fabedge支持无缝集成云端Calico插件，以后会扩展到其它插件。

# 联系我们
扫码加入微信交流群
<img src="https://user-images.githubusercontent.com/88021699/128297803-d08235b2-9c68-42da-8bd6-29cf8ba13559.png" width="30%">  